Readme
------

This is a simple filter module. It converts a Scripture reference into
a clickable link that points to online Bibles. This module works only for Hungarian references
and it is linked to the translations of the Online Bible from www.online-bible.ro.

It is a fork of the scripture module that works only for English. A
full list of credits is in the CREDITS file.


